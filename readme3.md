
For this class you will create pixel art.

You can see examples of pixel art here:

[Example 1](https://www.google.co.cr/imgres?imgurl=http%3A%2F%2Fpm1.narvii.com%2F6274%2F313f8e164ad4370b6eedf798f828785c85f6f7e8_00.jpg&imgrefurl=https%3A%2F%2Faminoapps.com%2Fc%2Fdibujos_animados%2Fpage%2Fblog%2Fcomo-hacer-pixel-art%2Fg02h_6uXazYzoXq08GJWv8J34gZD4m&tbnid=Lulm1Yn7pt5mGM&vet=12ahUKEwiZ_Jntt4z4AhVDAN8KHfatDVIQMygHegUIARDrAQ..i&docid=Nntl1c-ebB3xLM&w=443&h=332&q=pixel%20art&ved=2ahUKEwiZ_Jntt4z4AhVDAN8KHfatDVIQMygHegUIARDrAQ&safe=active&ssui=on)

[Example 2](https://www.google.co.cr/imgres?imgurl=https%3A%2F%2Fimages-na.ssl-images-amazon.com%2Fimages%2FI%2F61wxMMrrSWL.png&imgrefurl=https%3A%2F%2Fwww.amazon.com%2FPixel-Unicorn-Free-Number-Sandbox-Coloring%2Fdp%2FB07CJY87CS&tbnid=B3oBf3ZlwZOw8M&vet=12ahUKEwiZ_Jntt4z4AhVDAN8KHfatDVIQMygeegUIARCiAg..i&docid=S5DENQMnukQ3xM&w=512&h=512&q=pixel%20art&ved=2ahUKEwiZ_Jntt4z4AhVDAN8KHfatDVIQMygeegUIARCiAg&safe=active&ssui=on#imgrc=rW0YF7_NeWBy1M&imgdii=m9vBJSDCVxYsKM)

[Example 3](https://www.google.co.cr/imgres?imgurl=https%3A%2F%2Fplay-lh.googleusercontent.com%2Fsukn8y9sv7njWMI-sfXTDxVWUE7fzIUyqE0axQh0WAvOrmmU4FSr0y1O8EPSNbV97gs&imgrefurl=https%3A%2F%2Fplay.google.com%2Fstore%2Fapps%2Fdetails%3Fid%3Dcom.playlist.pablo%26hl%3Des%26gl%3DUS&tbnid=7ImljTw6TlE6bM&vet=12ahUKEwiZ_Jntt4z4AhVDAN8KHfatDVIQMygSegUIARCCAg..i&docid=dGfGF0UOkFNmLM&w=512&h=512&q=pixel%20art&ved=2ahUKEwiZ_Jntt4z4AhVDAN8KHfatDVIQMygSegUIARCCAg&safe=active&ssui=on)

[Example 4](https://www.google.co.cr/imgres?imgurl=http%3A%2F%2Fstore-images.s-microsoft.com%2Fimage%2Fapps.50744.13620573475464507.9a51f97d-cbd4-427c-a275-2c2017a591e8.87f95465-847f-4f84-a39f-f6a176ad99c6&imgrefurl=https%3A%2F%2Fwww.microsoft.com%2Fes-es%2Fp%2Fpixel-art-color-by-number-coloring-book%2F9nhznk1b1jsn&tbnid=jp0bnEgaxjgaIM&vet=12ahUKEwiZ_Jntt4z4AhVDAN8KHfatDVIQMygRegUIARCAAg..i&docid=gzwr61hwkUJg5M&w=300&h=300&q=pixel%20art&ved=2ahUKEwiZ_Jntt4z4AhVDAN8KHfatDVIQMygRegUIARCAAg&safe=active&ssui=on)

[Example 5](https://www.google.co.cr/imgres?imgurl=https%3A%2F%2Fcms-assets.tutsplus.com%2Fcdn-cgi%2Fimage%2Fwidth%3D600%2Fuploads%2Fusers%2F117%2Fposts%2F21759%2Ffinal_image%2Fpixelart.png&imgrefurl=https%3A%2F%2Fdesign.tutsplus.com%2Fes%2Farticles%2Fwhat-is-pixel-art--cms-21759&tbnid=YN9HVzuHjXeQeM&vet=12ahUKEwiZ_Jntt4z4AhVDAN8KHfatDVIQMygOegUIARD6AQ..i&docid=7tiVs_K7Za7tfM&w=600&h=222&q=pixel%20art&ved=2ahUKEwiZ_Jntt4z4AhVDAN8KHfatDVIQMygOegUIARD6AQ&safe=active&ssui=on)

[Example 6](https://www.google.co.cr/imgres?imgurl=http%3A%2F%2Fpm1.narvii.com%2F6274%2F313f8e164ad4370b6eedf798f828785c85f6f7e8_00.jpg&imgrefurl=https%3A%2F%2Faminoapps.com%2Fc%2Fdibujos_animados%2Fpage%2Fblog%2Fcomo-hacer-pixel-art%2Fg02h_6uXazYzoXq08GJWv8J34gZD4m&tbnid=Lulm1Yn7pt5mGM&vet=12ahUKEwiZ_Jntt4z4AhVDAN8KHfatDVIQMygHegUIARDrAQ..i&docid=Nntl1c-ebB3xLM&w=443&h=332&q=pixel%20art&ved=2ahUKEwiZ_Jntt4z4AhVDAN8KHfatDVIQMygHegUIARDrAQ&safe=active&ssui=on)

Think of something that you want to create in pixel art. Look for more examples on the internet. When you have decided, go to the following link to create your art:

https://www.pixelartcss.com/

You can change the size of the canvas(make a big image, for example 25 by 25). If you scroll down you will see an image with instructions on how to use this app. When you´re done, download the image and create another one. Make at least three designs. Turn in all the images.

The image that you create today will be created in minetest during class. Scroll down to see the eaxmple that I created.

![](pixelart00.png)

![](pixelart01.png)

![](pixelart02.png)
