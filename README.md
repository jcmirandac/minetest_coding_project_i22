# Minetest_coding_project_I22

Introduction to coding with minetest.

## Getting started

Create a new folder (mymod) for this practice. Inside "mymods" create another folder named "textures".

minetest - mods - mymod - textures

When you have the new folder, go back to classroom (https://classroom.google.com/c/NTIxOTIwMjk3Mjgz?cjc=7p7urtk) and download the two files from the posted assignment (init.lua and mod.conf).

Follow teacher instructions to move those two files into the folder previously created.

## Coding

We need to edit the files to create our mod. Open the file named **mod.conf** and add the text shown in the following image:

![text for the mod.conf file](/text_mod.png)

After typing the text save the file (important: do not change the name of the file, just save).

Now, you will create the code for your first block in minetest. Open the file named **init.lua** and add the text shown in the following image:

![text for the mod.conf file](/text_init.png)

After typing the text save the file (important: do not change the name of the file, just save).

## The image for your block

We are almost done with the code, but in this case, you must also create an image that will be used inside the game for your block.

In order to create the block image, go to this website: [piskelapp](https://www.piskelapp.com/).

<img src="/piskel_site.png" alt="piskelapp site start screen" width="500"/>

When you see the start screen, click on **create srpite**.

Before you start drawing, we need to resize the image. Click on the **resize button** (shown in the image below), and change the first number to 16, then press enter and click on **resize**.

<img src="/piskel_resize.png" alt="piskelapp resize canvas" width="500"/>

You can now start drawing your block. Use different tools and colors to make your design. If you change your mind you can delete and start over. On the image bellow you can see the block that I made as an example:

<img src="/my_block.png" alt="piskelapp my block example" width="500"/>

When you´re done, go ahead and save your image following the next steps.

Click on the button with a mountain (look at the image below), then click on the **PNG** tab and on the first **download** button.

<img src="/piskel_download.png" alt="piskelapp download image" width="500"/>

Now we need to find the image that was just downloaded. It most probably is in the **Downloads** folder. Go there and find the image file. Now change the name of the file to **block1.png**.

Finally, we need to move the file to the textures folder.

## Testing the mod

We are ready to test the mod. For that, open the game and click on **new**.

<img src="/minetest_start_screen.png" alt="minetest start screen" width="500"/>

Type a name for your new world and click on **create**.

<img src="/minetest_create_world.png" alt="create a new world" width="500"/>

Click on **select mods** and find **mymod** and click it. Then enable the mod (at the top of the screen) and click on **save**.

<img src="/minetest_activate_mymod.png" alt="activate my mod" width="500"/>

Back on the first screen, activate **creative mode**, then click on your world and **play game**.

<img src="/minetest_play_game.png" alt="start playing your world" width="500"/>

In the game, press "i" (or "e") to show the inventory and look for your block.

<img src="/minetest_myblock.png" alt="my block inside the game" width="500"/>

---------------------------------------------------------------------------------------------------

## Authors and acknowledgment
Minetest is an open source voxel game engine.

© 2015-2022 The Minetest Team. Source
MIT for code, CC-BY-SA 3.0 for media and content. Font Awesome icons under CC-BY 4.0.

https://www.minetest.net/
