# Minetest_coding_project_I22

Introduction to coding with minetest.

## Getting started

On your computer, create a new folder **mymod** for this practice. Inside **mymod** create the folder **textures**. If you don´t know how to create folders, just save your files in the **Downloads** folder.

When you´re done, right-click the following link and open in a new tab: [editpad.online](https://www.editpad.online/?target=_blank)

## Coding

**Important**: when coding, you must not add or remove any character, and copy the code exactly as you see it in the examples. Do not change any of the symbols or the case of any letter.

On editpad, type the code shown in the image below:

![text for the mod.conf file](/text_mod.png)

After typing the text, scroll down to where it says "specify a file name..." and type **mod.conf** and click on **Download file**.

Now, you will create another file with editpad (refresh the page to start over in editpad). Type the code shown in the image below:

![text for the mod.conf file](/text_init.png)

After typing the text, scroll down to where it says "specify a file name..." and type **init.lua** and click on **Download file**.

Move both files to **mymod** folder.

## The image for your block

We are almost done with the code, but in this case, you must also create an image that will be used inside the game for your block.

In order to create the block image, go to this website: [piskelapp](https://www.piskelapp.com/).

<img src="/piskel_site.png" alt="piskelapp site start screen" width="500"/>

When you see the start screen, click on **create srpite**.

Before you start drawing, we need to resize the image. Click on the **resize button** (shown in the image below), and change the first number to 16, then press enter and click on **resize**.

<img src="/piskel_resize.png" alt="piskelapp resize canvas" width="500"/>

You can now start drawing your block. Use different tools and colors to make your design. If you change your mind you can delete and start over. On the image bellow you can see the block that I made as an example:

<img src="/my_block.png" alt="piskelapp my block example" width="500"/>

When you´re done, go ahead and save your image following the next steps.

Click on the button with a mountain (look at the image below), then click on the **PNG** tab and on the first **download** button.

<img src="/piskel_download.png" alt="piskelapp download image" width="500"/>

Now we need to find the image that was just downloaded. It most probably is in the **Downloads** folder. Go there and find the image file. Now change the name of the file to **block1.png**.

Finally, we need to move the file to the textures folder.

When you are done, turn in the three files (init.lua, mod.conf, block1.png)

## Creating more blocks

Now, go ahead and create two more blocks. Create the images and download the **png** files. The names should be **block2.png** and **block2.png**. Move those files to the textures folder.

Go back to editing the init.lua file and follow the example in the image below:

![text for the mod.conf file](/text_init2.png)

You must do something similar to add the third block. Save the files and turn them in.

## Testing the mod (next time we are in the lab)

We are ready to test the mod. For that, open the game and click on **new**.

<img src="/minetest_start_screen.png" alt="minetest start screen" width="500"/>

Type a name for your new world and click on **create**.

<img src="/minetest_create_world.png" alt="create a new world" width="500"/>

Click on **select mods** and find **mymod** and click it. Then enable the mod (at the top of the screen) and click on **save**.

<img src="/minetest_activate_mymod.png" alt="activate my mod" width="500"/>

Back on the first screen, activate **creative mode**, then click on your world and **play game**.

<img src="/minetest_play_game.png" alt="start playing your world" width="500"/>

In the game, press "i" (or "e") to show the inventory and look for your block.

<img src="/minetest_myblock.png" alt="my block inside the game" width="500"/>

---------------------------------------------------------------------------------------------------

## Authors and acknowledgment
Minetest is an open source voxel game engine.

© 2015-2022 The Minetest Team. Source
MIT for code, CC-BY-SA 3.0 for media and content. Font Awesome icons under CC-BY 4.0.

https://www.minetest.net/
